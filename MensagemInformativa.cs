﻿using System.Web.Mvc;

namespace Cabecao.MvcExtensions {
	public static class MensagemInformativaViewType {
		public static ViewResult MensagemInformativa(this Controller ctl, string titulo, string mensagem) {
			return MensagemInformativa(ctl, titulo, mensagem, "/", 5, titulo, null);
		}

		public static ViewResult MensagemInformativa(this Controller ctl, string titulo, string mensagem, string urlRedir, int timeOut) {
			return MensagemInformativa(ctl, titulo, mensagem, urlRedir, timeOut, titulo, null);
		}

		public static ViewResult MensagemInformativa(this Controller ctl, string titulo, string mensagem, int timeOut) {
			return MensagemInformativa(ctl, titulo, mensagem, "/", timeOut, titulo, null);
		}

		public static ViewResult MensagemInformativa(this Controller ctl, string titulo, string mensagem, string urlRedir) {
			return MensagemInformativa(ctl, titulo, mensagem, urlRedir, 0, titulo, null);
		}

		public static ViewResult MensagemInformativa(this Controller ctl, string titulo, string mensagem, string urlRedir, string titPag) {
			return MensagemInformativa(ctl, titulo, mensagem, urlRedir, 0, titulo, null);
		}

		public static ViewResult MensagemInformativa(this Controller ctl, string titulo, string mensagem, string urlRedir, int timeOut, string titPag, string masterPage) {
			ViewResult vw = new ViewResult();
			vw.ViewName = "MsgInformativa";
			if (masterPage != null) vw.MasterName = masterPage;
			vw.ViewData = new ViewDataDictionary(
				new MsgInfo(
					titulo,
					mensagem,
					urlRedir,
					titPag,
					timeOut
				));

			foreach (string vdk in ctl.ViewData.Keys) if (!vw.ViewData.ContainsKey(vdk)) vw.ViewData.Add(vdk, ctl.ViewData[vdk]);

			return vw;
		}
	}

	public class MsgInfo {
		private string _pageTitle;
		private string _msgTitle;
		private string _msgTxt;
		private string _urlRedir;
		private int _timeOut;

		public MsgInfo(string titMensagem, string msgTxt) : this(titMensagem, msgTxt, "/", titMensagem, 5) { }

		public MsgInfo(string titMensagem, string msgTxt, string titPag) : this(titMensagem, msgTxt, "/", titPag, 5) { }

		public MsgInfo(string titMensagem, string msgTxt, string urlRedir, int timeOut) : this(titMensagem, msgTxt, urlRedir, titMensagem, timeOut) { }

		public MsgInfo(string titMensagem, string msgTxt, int timeOut) : this(titMensagem, msgTxt, "/", titMensagem, timeOut) { }

		public MsgInfo(string titMensagem, string msgTxt, string urlRedir, string titPag, int timeOut) {
			_pageTitle = titPag;
			_msgTitle = titMensagem;
			_msgTxt = msgTxt;
			_urlRedir = urlRedir;
			_timeOut = timeOut;
		}

		public string TituloPagina {
			get { return _pageTitle; }
			set { _pageTitle = value; }
		}

		public string TituloMensagem {
			get { return _msgTitle; }
			set { _msgTitle = value; }
		}

		public string Mensagem {
			get { return _msgTxt; }
			set { _msgTxt = value; }
		}

		public string URLRedir {
			get { return _urlRedir; }
			set { _urlRedir = value; }
		}

		public int TimeOut {
			get { return _timeOut; }
			set { _timeOut = value; }
		}
	}
}
