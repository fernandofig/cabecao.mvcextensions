﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Cabecao.MvcExtensions {
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public abstract class AutorizarBaseAttribute : AuthorizeAttribute {
		public AutorizarBaseAttribute() : base() { }

		protected void CacheValidateHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus) {
			validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
		}

		public abstract override void OnAuthorization(AuthorizationContext filterContext);

		protected void SetCachePolicy(AuthorizationContext filterContext) {
			// ** IMPORTANT **
			// Since we're performing authorization at the action level, the authorization code runs
			// after the output caching module. In the worst case this could allow an authorized user
			// to cause the page to be cached, then an unauthorized user would later be served the
			// cached page. We work around this by telling proxies not to cache the sensitive page,
			// then we hook our custom authorization code into the caching mechanism so that we have
			// the final say on whether a page should be served from the cache.
			HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
			cachePolicy.SetProxyMaxAge(new TimeSpan(0));
			cachePolicy.AddValidationCallback(CacheValidateHandler, null /* data */);
		}
	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public class AutorizarAttribute : AutorizarBaseAttribute {
		/// <summary>
		/// The name of the master page or view to use when rendering the view on authorization failure.  Default
		/// is null, indicating to use the master page of the specified view.
		/// </summary>
		public virtual string MasterName { get; set; }

		/// <summary>
		/// The name of the view to render on authorization failure.  Default is "Error".
		/// </summary>
		public virtual string ViewName { get; set; }

		public virtual string LoginUrl { get; set; }

		public virtual bool DisableReturnUrl { get; set; }

		public virtual bool RedirectToLoginOnAuthFailure { get; set; }

		public AutorizarAttribute()	: base() {
			this.ViewName = "Error";
			this.LoginUrl = FormsAuthentication.LoginUrl;
			this.DisableReturnUrl = false;
			this.RedirectToLoginOnAuthFailure = true;
		}

		public override void OnAuthorization(AuthorizationContext filterContext) {
			if (filterContext == null) {
				throw new ArgumentNullException("filterContext");
			}

			if (AuthorizeCore(filterContext.HttpContext)) {
				SetCachePolicy(filterContext);
			} else if (!filterContext.HttpContext.User.Identity.IsAuthenticated) {
				// auth failed, redirect to login page
				if (this.MasterName != null)
					filterContext.HttpContext.Cache.Insert("loginMP!" + filterContext.HttpContext.Session.SessionID, this.MasterName);
				else
					filterContext.HttpContext.Cache.Remove("loginMP!" + filterContext.HttpContext.Session.SessionID);

				if (this.LoginUrl.Substring(0, 1) != "~") this.LoginUrl = "~" + this.LoginUrl;

				if (filterContext.HttpContext.Request != null && !this.DisableReturnUrl) {
					this.LoginUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(filterContext.HttpContext
															 .Request
															 .Url
															 .AbsoluteUri);
				}

				if (this.RedirectToLoginOnAuthFailure)
					filterContext.Result = new RedirectResult(this.LoginUrl);
				else {
					//filterContext.Result = new HttpUnauthorizedResult();
					//filterContext.Result = new HttpStatusCodeResult(401);
					filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
					filterContext.HttpContext.Response.StatusCode = 401;
					filterContext.HttpContext.Response.Write("Não autenticado");
					filterContext.HttpContext.Response.Flush();
					filterContext.HttpContext.Response.End();
				}

			} else if (filterContext.HttpContext.User.IsInRole("SuperUser")) {
				// is authenticated and is in the SuperUser role
				SetCachePolicy(filterContext);
			} else {
				ViewDataDictionary viewData = new ViewDataDictionary();
				viewData.Add("Msg", "Acesso negado: Seu login não possui privilégios suficientes para acessar o conteúdo solicitado.");
				filterContext.Result = new ViewResult { MasterName = this.MasterName, ViewName = this.ViewName, ViewData = viewData };
			}
		}
	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public class AutorizarAJAXAttribute : AutorizarBaseAttribute {
		public AutorizarAJAXAttribute() : base() { }

		public override void OnAuthorization(AuthorizationContext filterContext) {
			if (filterContext == null) throw new ArgumentNullException("filterContext");

			if (AuthorizeCore(filterContext.HttpContext)) {
				SetCachePolicy(filterContext);
			} else if (!filterContext.HttpContext.User.Identity.IsAuthenticated) {
				//filterContext.Result = new HttpUnauthorizedResult();
				//filterContext.Result = new HttpStatusCodeResult(401);
				filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
				filterContext.HttpContext.Response.StatusCode = 401;
				filterContext.HttpContext.Response.Write("Não autenticado");
				filterContext.HttpContext.Response.Flush();
				filterContext.HttpContext.Response.End();

			} else if (filterContext.HttpContext.User.IsInRole("SuperUser")) {
				// is authenticated and is in the SuperUser role
				SetCachePolicy(filterContext);
			} else {
				filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
				filterContext.HttpContext.Response.StatusCode = 403;
				filterContext.HttpContext.Response.Write("Não permitido");
				filterContext.HttpContext.Response.Flush();
				filterContext.HttpContext.Response.End();
			}
		}
	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public class AutorizarUsuariosReaisAttribute : AutorizarAttribute {
		public AutorizarUsuariosReaisAttribute() : base() { }

		public override void OnAuthorization(AuthorizationContext filterContext) {
			if (filterContext == null) {
				throw new ArgumentNullException("filterContext");
			}

			if (filterContext.HttpContext.User.Identity.Name.ToUpper() == "ADMIN") {
				ViewDataDictionary viewData = new ViewDataDictionary();
				viewData.Add("Msg", "<p>ACESSO NÃO APLICÁVEL</p><p>O Acesso à este recurso não é possível para o usuário ADMIN - somente usuários reais do sistema podem ter acesso ao conteúdo solicitado.</p>");
				filterContext.Result = new ViewResult { MasterName = this.MasterName, ViewName = "Error", ViewData = viewData };
			} else {
				base.OnAuthorization(filterContext);
			}
		}
	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public class AutorizarAJAXUsuariosReaisAttribute : AutorizarAJAXAttribute {
		public AutorizarAJAXUsuariosReaisAttribute() : base() { }

		public override void OnAuthorization(AuthorizationContext filterContext) {
			if (filterContext == null) throw new ArgumentNullException("filterContext");

			if (filterContext.HttpContext.User.Identity.Name.ToUpper() == "ADMIN") {
				filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
				filterContext.HttpContext.Response.StatusCode = 403;
				filterContext.HttpContext.Response.Write("O Usuário ADMIN não tem permissão de acesso à esta chamada.");
				filterContext.HttpContext.Response.Flush();
				filterContext.HttpContext.Response.End();
			} else {
				base.OnAuthorization(filterContext);
			}
		}
	}
}