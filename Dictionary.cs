﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Cabecao.MvcExtensions {
	public static class DictionaryExtensions {
		public static SelectList GetSelectList(this Dictionary<string, string> list, string sel, bool hasEmptyOption, string emptySelecOptionText = "-- selecione --") {
			IList<object> selList = list.Select(o => (object)new { Id = o.Key, Text = o.Value }).ToList();

			if (string.IsNullOrWhiteSpace(sel)) {
				if (hasEmptyOption) selList.Insert(0, new { Id = "", Text = emptySelecOptionText });
				return new SelectList(selList, "Id", "Text");
			} else
				return new SelectList(selList, "Id", "Text", sel);
		}
	}
}
