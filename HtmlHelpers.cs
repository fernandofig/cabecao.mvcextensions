﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

using Cabecao.TypesExtensions;

namespace Cabecao.MvcExtensions.HtmlHelpers {
	public static class ContentFmt {
		public static IHtmlString OptionalContent(this HtmlHelper hlp, object content) {
			if (content == null || string.IsNullOrWhiteSpace(content.ToString())) { 
				return hlp.Raw("<em>n/d</em>");
			} else {
				return hlp.Raw(content != null ? content.ToString() : null);
			}
		}

		public static IHtmlString TruncateWithTooltip(this HtmlHelper hlp, string txt, int size) {
			if (txt != null && txt.Length > size) {
				return hlp.Raw("<span class=\"hinttext\" title=\"" + txt + "\">" + StringExtensions.TruncaString(txt, size) + "</span>");
			} else {
				return hlp.Raw(txt);
			}
		}

		public static IHtmlString TooltipedText(this HtmlHelper hlp, string txt, string tooltip) {
			return (string.IsNullOrWhiteSpace(tooltip) ? hlp.Raw(txt) : hlp.Raw("<span class=\"hinttext\" title=\"" + tooltip + "\">" + txt + "</span>"));
		}

		public static IHtmlString TooltipedTruncateText(this HtmlHelper hlp, string txt, string tooltip, int size) {
			return (string.IsNullOrWhiteSpace(tooltip) ?
				TruncateWithTooltip(hlp, txt, size) :
				hlp.Raw("<span class=\"hinttext\" title=\"" + tooltip + "\">" + StringExtensions.TruncaString(txt, size) + "</span>")
			);
		}

		public static IHtmlString InflectByQty(this HtmlHelper hlp, int qtd, string singular, string plural) {
			return hlp.Raw((qtd < 2 ? singular : plural));
		}
	}
}

namespace Cabecao.MvcExtensions.HtmlHelpers.Bootstrap {
	public static class BootstrapFields {
		public static IHtmlString BSText(this HtmlHelper hlp, string name, string label, GridSize sizing, object htmlAttributes = null, object labelHtmlAttributes = null, object ctlContainerHtmlAttributes = null) {
			string labelAttribs = MergeAttributes(new { @class = "col-md-{2} control-label" }, labelHtmlAttributes);
			string ctAttribs = MergeAttributes(new { @class = "col-md-{3}" }, ctlContainerHtmlAttributes);

			string htmlString = @"
				<div {6}>
					{4}
				</div>";

			if (label != null) htmlString = @"<label for=""{0}"" {5}>{1}</label>" + "\n" + htmlString;

			string inputField = hlp.TextBox(name, hlp.ViewData[name], MergeAObject(new { @class = "form-control" }, htmlAttributes)).ToHtmlString();

			return hlp.Raw(string.Format(htmlString, name, label, sizing.Label, sizing.Field, inputField, labelAttribs, ctAttribs));
		}

		public static IHtmlString BSDropDown(this HtmlHelper hlp, string name, string label, GridSize sizing, object htmlAttributes = null, object labelHtmlAttributes = null, object ctlContainerHtmlAttributes = null) {
			string labelAttribs = MergeAttributes(new { @class = "col-md-{2} control-label" }, labelHtmlAttributes);
			string ctAttribs = MergeAttributes(new { @class = "col-md-{3}" }, ctlContainerHtmlAttributes);

			string htmlString = @"
				<div {6}>
					{4}
				</div>";

			if (label != null) htmlString = @"<label for=""{0}"" {5}>{1}</label>" + "\n" + htmlString;

			string inputField = hlp.DropDownList(name, null, MergeAObject(new { @class = "form-control" }, htmlAttributes)).ToHtmlString();

			return hlp.Raw(string.Format(htmlString, name, label, sizing.Label, sizing.Field, inputField, labelAttribs, ctAttribs));
		}

		public static IHtmlString BSCheckbox(this HtmlHelper hlp, string name, string label, string value, GridSize sizing, object htmlAttributes = null, object labelHtmlAttributes = null) {
			string labelAttribs = MergeAttributes(new { @class = "col-md-{3} control-label" }, labelHtmlAttributes);
			string controlAttribs = MergeAttributes(htmlAttributes, null);

			string htmlString = @"
				<label for=""{0}"" {6}>
					<input type=""checkbox"" name=""{0}"" id=""{0}"" value=""{2}"" {4} {5}>
					{1}
				</label>";

			object modelValueObj = hlp.ViewData[name];

			if (modelValueObj == null && hlp.ViewData.Model != null) {
				PropertyInfo mProp = hlp.ViewData.Model.GetType().GetProperty(name);

				if (mProp != null)
					modelValueObj = mProp.GetValue(hlp.ViewData.Model, null);
			}

			return hlp.Raw(string.Format(htmlString, name, label, value, sizing.Label, controlAttribs, (modelValueObj != null && modelValueObj.ToString().ToLower() == "true" ? "checked" : ""), labelAttribs));
		}

		public static IHtmlString BSDatePicker(this HtmlHelper hlp, string name, string label, GridSize sizing, object htmlAttributes = null, object labelHtmlAttributes = null, object ctlContainerHtmlAttributes = null) {
			string labelAttribs = MergeAttributes(new { @class = "col-md-{2} control-label" }, labelHtmlAttributes);
			string ctAttribs = MergeAttributes(new { @class = "col-md-{3}" }, ctlContainerHtmlAttributes);

			string htmlString = @"
				<div {6}>
					<div class=""input-group date"">
						{4}
						<div class=""input-group-addon"">
							<span class=""glyphicon glyphicon-th""></span>
						</div>
					</div>
				</div>";

			if (label != null) htmlString = @"<label for=""{0}"" {5}>{1}</label>" + "\n" + htmlString;

			string inputField = hlp.TextBox(name, hlp.ViewData[name], MergeAObject(new { @class = "form-control" }, htmlAttributes)).ToHtmlString();

			return hlp.Raw(string.Format(htmlString, name, label, sizing.Label, sizing.Field, inputField, labelAttribs, ctAttribs));
		}

		public static IHtmlString BSStaticText(this HtmlHelper hlp, object text, string label, GridSize sizing, object htmlAttributes = null, object ctlContainerHtmlAttributes = null) {
			string ctlAttribs = MergeAttributes(new { @class = "form-control-static" }, htmlAttributes);
			string ctAttribs = MergeAttributes(new { @class = "col-md-{3}" }, ctlContainerHtmlAttributes);

			string htmlString = @"
				<div {5}>
					<p {4}>{0}</p>
				</div>";

			if (label != null) htmlString = @"<label class=""col-md-{2} control-label"">{1}</label>" + "\n" + htmlString;

			return hlp.Raw(string.Format(htmlString, text.ToString(), label, sizing.Label, sizing.Field, ctlAttribs, ctAttribs));
		}

		private static string MergeAttributes(object items1, object items2) {
			Dictionary<string, object> attribs = MergeAObject(items1, items2);
			string attribElements = "";

			foreach (KeyValuePair<string,object> attrib in attribs) attribElements += attrib.Key + "=\"" + attrib.Value + "\" ";

			return attribElements;
		}

		private static Dictionary<string, object> MergeAObject(object item1, object item2) {
			RouteValueDictionary dic1 = new RouteValueDictionary(item1);
			RouteValueDictionary dic2 = new RouteValueDictionary(item2);

			foreach (var item in dic2) {
				if (dic1.ContainsKey(item.Key)) {
					dic1[item.Key] += " " + item.Value;
				} else {
					dic1.Add(item.Key, item.Value);
				}
			}

			var a = dic1.ToDictionary(k => k.Key, v => v.Value);

			return dic1.ToDictionary(k => k.Key, v => v.Value);
		}
	}

	public class GridSize {
		public GridSize(int size) {
			this.Label = size;
			this.Field = size;
		}

		public GridSize(int sizeLabel, int sizeField) {
			this.Label = sizeLabel;
			this.Field = sizeField;
		}

		public static GridSize As(int size) {
			return new GridSize(size);
		}

		public static GridSize As(int sizeLabel, int sizeField) {
			return new GridSize(sizeLabel, sizeField);
		}

		public int Label { get; set; }
		public int Field { get; set; }
	}
}