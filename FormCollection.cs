﻿using System;
using System.Web.Mvc;

using Cabecao.TypesExtensions;

using JS = Microsoft.JScript.GlobalObject;

namespace Cabecao.MvcExtensions {
	public static class FormCollectionExtensions {
		public static void SanitizeFields(this FormCollection frmFlds) { SanitizeFields(frmFlds, null, null); }

		public static void SanitizeFields(this FormCollection frmFlds, string[] fieldNamePatternsNotToTrim, string[] fieldsNotToNull) {
			bool fazTrim;
			bool fazNull;

			foreach (string campo in frmFlds.AllKeys) {
				fazTrim = true;
				fazNull = true;
				if (fieldNamePatternsNotToTrim != null && fieldNamePatternsNotToTrim.Length > 0) {
					//Um Array.Find ou similar seria melhor, mas não tenho tempo de ver a sintaxe disso agora...
					foreach (string umPattCampo in fieldNamePatternsNotToTrim)
						if (campo.IndexOf(umPattCampo, 0, StringComparison.OrdinalIgnoreCase) >= 0) fazTrim = false;
				}

				if (fieldsNotToNull != null && fieldsNotToNull.Length > 0) {
					foreach (string campoNaoNular in fieldsNotToNull)
						if (string.Equals(campo, campoNaoNular, StringComparison.OrdinalIgnoreCase)) fazNull = false;

				}

				if (frmFlds.GetValues(campo).Length == 1) frmFlds[campo] = frmFlds[campo].NormalizaEspaco(fazTrim, fazNull);
			}
		}

		public static FormCollection FilterFields(this FormCollection frmFlds, string patternPrefix = null, string patternSuffix = null, bool excludeFilter = false) {
			if (patternPrefix == null && patternSuffix == null) return frmFlds;

			FormCollection newCol = new FormCollection(frmFlds);

			foreach (string campo in frmFlds.AllKeys) {
				if (!string.IsNullOrEmpty(patternPrefix)) {
					if((!excludeFilter && !campo.StartsWith(patternPrefix, StringComparison.InvariantCultureIgnoreCase)) ||
					(excludeFilter && campo.StartsWith(patternPrefix, StringComparison.InvariantCultureIgnoreCase)))
						newCol.Remove(campo);
				}

				if (!string.IsNullOrEmpty(patternSuffix)) {
					if ((!excludeFilter && !campo.EndsWith(patternSuffix, StringComparison.InvariantCultureIgnoreCase)) ||
					(excludeFilter && campo.EndsWith(patternSuffix, StringComparison.InvariantCultureIgnoreCase)))
						newCol.Remove(campo);
				}
			}

			return newCol;
		}

		public static T GetValueAs<T>(this FormCollection frmFlds, string key) {
			Type ft = typeof(T);
			object val;

			if (ft == typeof(int))
				val = Int32.Parse(frmFlds[key]);
			else if (ft == typeof(decimal))
				val = Decimal.Parse(frmFlds[key]);
			else if (ft == typeof(double))
				val = Double.Parse(frmFlds[key]);
			else if (ft == typeof(DateTime))
				val = DateTime.Parse(frmFlds[key]);
			else
				val = Convert.ChangeType(frmFlds[key], typeof(T));
			
			return (T)val;
		}
	}

	public static class JSExt {
		/// <summary>
		/// Same as Microsoft.JScript.GlobalObject.unescape(), except this trims leading/trailing spaces on the input string and normalizes it to null if the trimmed result is an empty string, in which case this method will also return null, instead of the original implementation that returns an 'undefined' string when the input is a null reference.
		/// </summary>
		public static string unescape(string @string) {
			string input = @string.NormalizaEspaco();
			if (input == null)
				return null;
			else
				return JS.escape(input);
		}

		/// <summary>
		/// Same as Microsoft.JScript.GlobalObject.escape(), except this checks whether the input string is a null reference, in which case the input string is normalized to an empty string and then that is escape()'ed, instead of the original implementation that returns an 'undefined' string when the input is a null reference.
		/// </summary>
		public static string escape(string @string) {
			string input = @string == null ? "" : @string;
			return JS.escape(input);
		}
	}
}