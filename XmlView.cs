﻿using System.Web.Mvc;
using System.Xml;

namespace Cabecao.MvcExtensions {
	public static class XmlViewType {
		public static ViewResult XmlView(this Controller ctl, string xd) {
			if (xd.Length > 6 && xd.Substring(0, 6) != "<?xml ") xd = "<?xml version=\"1.0\"?>\r\n" + xd;

			ViewResult vw = new ViewResult();
			vw.ViewName = "XmlResult";
			vw.ViewData = new ViewDataDictionary();
			vw.ViewData.Model = xd;

			ctl.Response.ContentEncoding = System.Text.Encoding.UTF8;
			ctl.Response.ContentType = "text/xml";

			return vw;
		}

		public static ViewResult XmlView(this Controller ctl, XmlDocument xd) {
			if (xd.OuterXml.Length > 6 && xd.OuterXml.Substring(0, 6) != "<?xml ") xd.PrependChild(xd.CreateXmlDeclaration("1.0", null, null));


			ViewResult vw = new ViewResult();
			vw.ViewName = "XmlResult";
			vw.ViewData = new ViewDataDictionary();
			vw.ViewData.Model = xd;

			ctl.Response.ContentEncoding = System.Text.Encoding.UTF8;
			ctl.Response.ContentType = "text/xml";

			return vw;
		}
	}
}
